package ru.fedun.tm.controller;

import ru.fedun.tm.api.controller.IUserController;
import ru.fedun.tm.api.service.IAuthService;
import ru.fedun.tm.api.service.IUserService;
import ru.fedun.tm.entity.User;
import ru.fedun.tm.exception.notfound.UserNotFoundException;
import ru.fedun.tm.exception.user.AccessDeniedException;
import ru.fedun.tm.role.Role;
import ru.fedun.tm.util.TerminalUtil;

public class UserController implements IUserController {

    private final IUserService userService;

    private final IAuthService authService;

    public UserController(final IUserService userService, final IAuthService authService) {
        this.userService = userService;
        this.authService = authService;
    }

    @Override
    public void showUserInfo() {
        System.out.println("[SHOW PROFILE INFO]");
        final String userId = authService.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        final User user = userService.findById(userId);
        if (user == null) throw new UserNotFoundException();

        final String login = user.getLogin();
        if (login == null || login.isEmpty()) System.out.println("Login: Unknown");
        else System.out.println("Login: " + login);

        final String firstName = user.getFirstName();
        if (firstName == null || firstName.isEmpty()) System.out.println("First name: Unknown");
        else System.out.println("First Name: " + firstName);

        final String secondName = user.getSecondName();
        if (secondName == null || secondName.isEmpty()) System.out.println("Second name: Unknown");
        else  System.out.println("Second Name: " + secondName);

        final String email = user.getEmail();
        if (email == null || email.isEmpty()) System.out.println("E-mail: Unknown");
        else System.out.println("E-mail: " + email);

        final Role role = user.getRole();
        System.out.println("Role " + role.getDisplayName());
        System.out.println("[OK]");
        System.out.println();
    }

    @Override
    public void deleteProfile() {
        System.out.println("[DELETE PROFILE]");
        System.out.println("DO YOU REALLY WANT DELETE PROFILE?");
        System.out.println("1 - YES, 2 - NO");
        final Integer answer = TerminalUtil.nextInt();
        if (answer.equals(1)) {
            final String userId = authService.getUserId();
            final User user = userService.findById(userId);
            userService.removeUser(userId, user);
        } else return;
        System.out.println("[OK]");
        System.out.println();
    }

    @Override
    public void changePassword() {
        System.out.println("[CHANGE PASSWORD]");
        final String userId = authService.getUserId();
        System.out.println("[ENTER OLD PASSWORD:]");
        final String oldPassword = TerminalUtil.nextLine();
        System.out.println("[ENTER NEW PASSWORD:]");
        final String newPassword = TerminalUtil.nextLine();
        userService.updatePassword(userId, oldPassword, newPassword);
        System.out.println("[OK]");
        System.out.println();
    }

    @Override
    public void changeEmail() {
        System.out.println("[CHANGE EMAIL]");
        final String userId = authService.getUserId();
        System.out.println("[ENTER NEW EMAIL:]");
        final String newEmail = TerminalUtil.nextLine();
        userService.updateMail(userId, newEmail);
        System.out.println("[OK]");
        System.out.println();
    }

}
