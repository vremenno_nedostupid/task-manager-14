package ru.fedun.tm.controller;

import ru.fedun.tm.api.controller.ICrudController;
import ru.fedun.tm.api.service.IAuthService;
import ru.fedun.tm.api.service.ICrudService;
import ru.fedun.tm.entity.Task;
import ru.fedun.tm.util.TerminalUtil;

import java.util.List;

public class TaskController implements ICrudController<Task> {

    private final ICrudService<Task> taskService;

    private final IAuthService authService;

    public TaskController(
            final ICrudService<Task> taskService,
            final IAuthService authService
    ) {
        this.taskService = taskService;
        this.authService = authService;
    }

    public void showAll() {
        System.out.println("[LIST TASKS]");
        final String userId = authService.getUserId();
        final List<Task> tasks = taskService.findAll(userId);
        int index = 1;
        for (Task task : tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
        System.out.println("[OK]");
        System.out.println();
    }

    public void clear() {
        System.out.println("[TASK CLEAR]");
        final String userId = authService.getUserId();
        taskService.clear(userId);
        System.out.println("[OK]");
        System.out.println();
    }

    public void create() {
        System.out.println("[CREATE TASK]");
        final String userId = authService.getUserId();
        System.out.println("[ENTER TITLE:]");
        final String title = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION:]");
        final String description = TerminalUtil.nextLine();
        taskService.create(userId, title, description);
        System.out.println("[OK]");
        System.out.println();
    }

    @Override
    public void showOneById() {
        System.out.println("[SHOW TASK]");
        final String userId = authService.getUserId();
        System.out.println("[ENTER ID:]");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.getOneById(userId, id);
        if (task == null) System.out.println("[FAIL]");
        else showTask(task);
    }

    @Override
    public void showOneByIndex() {
        System.out.println("[SHOW TASK]");
        final String userId = authService.getUserId();
        System.out.println("[ENTER INDEX:]");
        final Integer index = TerminalUtil.nextInt() - 1;
        final Task task = taskService.getOneByIndex(userId, index);
        if (task == null) System.out.println("[FAIL]");
        else showTask(task);
        System.out.println();
    }

    @Override
    public void showOneByTitle() {
        System.out.println("[SHOW TASK]");
        final String userId = authService.getUserId();
        System.out.println("[ENTER TITLE:]");
        final String title = TerminalUtil.nextLine();
        final Task task = taskService.getOneByTitle(userId, title);
        if (task == null) System.out.println("[FAIL]");
        else showTask(task);
    }

    public void showTask(final Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("TITLE: " + task.getTitle());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("[OK]");
    }

    @Override
    public void updateOneByIndex() {
        System.out.println("[UPDATE TASK]");
        final String userId = authService.getUserId();
        System.out.println("[ENTER INDEX:]");
        final Integer index = TerminalUtil.nextInt();
        final Task task = taskService.getOneByIndex(userId, index);
        if (task == null) {
            System.out.println("[FAIL]");
        }
        System.out.println("ENTER TITLE:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdated = taskService.updateByIndex(userId, index, name, description);
        if (taskUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void updateOneById() {
        System.out.println("[UPDATE TASK]");
        final String userId = authService.getUserId();
        System.out.println("[ENTER ID:]");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.getOneById(userId, id);
        if (task == null) {
            System.out.println("[FAIL]");
        }
        System.out.println("ENTER TITLE:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdated = taskService.updateById(userId, id, name, description);
        if (taskUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void removeOneById() {
        System.out.println("[REMOVE TASK]");
        final String userId = authService.getUserId();
        System.out.println("[ENTER ID:]");
        final String id = TerminalUtil.nextLine();
        if (id == null || id.isEmpty()) {
            System.out.println("[FAIL]");
            return;
        }
        final Task task = taskService.removeOneById(userId, id);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void removeOneByIndex() {
        System.out.println("[REMOVE TASK]");
        final String userId = authService.getUserId();
        System.out.println("[ENTER INDEX:]");
        final Integer index = TerminalUtil.nextInt();
        if (index <= 0) {
            System.out.println("[FAIL]");
            return;
        }
        final Task task = taskService.removeOneByIndex(userId, index);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void removeOneByTitle() {
        System.out.println("[REMOVE TASK]");
        final String userId = authService.getUserId();
        System.out.println("[ENTER TITLE:]");
        final String title = TerminalUtil.nextLine();
        if (title == null || title.isEmpty()) {
            System.out.println("[FAIL]");
            return;
        }
        final Task task = taskService.removeOneByTitle(userId, title);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }
}
