package ru.fedun.tm.controller;

import ru.fedun.tm.api.controller.IAuthController;
import ru.fedun.tm.api.service.IAuthService;
import ru.fedun.tm.util.TerminalUtil;

public class AuthController implements IAuthController {

    private final IAuthService authService;

    public AuthController(final IAuthService authService) {
        this.authService = authService;
    }

    @Override
    public void login() {
        System.out.println("[LOGIN]");
        System.out.println("[ENTER LOGIN:]");
        final String login = TerminalUtil.nextLine();
        System.out.println("[ENTER PASSWORD:]");
        final String password = TerminalUtil.nextLine();
        authService.login(login, password);
        System.out.println("[OK]");
        System.out.println();
    }

    @Override
    public void logout() {
        System.out.println("[LOGOUT]");
        authService.logout();
        System.out.println("[OK]");
        System.out.println();
    }

    @Override
    public void registry() {
        System.out.println("[REGISTRY]");
        System.out.println("[ENTER LOGIN:]");
        final String login = TerminalUtil.nextLine();
        System.out.println("[ENTER PASSWORD:]");
        final String password = TerminalUtil.nextLine();
        System.out.println("[ENTER FIRST NAME:]");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("[ENTER LAST NAME:]");
        final String lastName = TerminalUtil.nextLine();
        System.out.println("[ENTER E-MAIL:]");
        final String email = TerminalUtil.nextLine();
        authService.registry(login, password, firstName, lastName, email);
        System.out.println("[OK]");
        System.out.println();
    }
}
