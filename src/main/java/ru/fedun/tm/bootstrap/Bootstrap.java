package ru.fedun.tm.bootstrap;

import ru.fedun.tm.api.controller.IAuthController;
import ru.fedun.tm.api.controller.ICommandController;
import ru.fedun.tm.api.controller.ICrudController;
import ru.fedun.tm.api.controller.IUserController;
import ru.fedun.tm.api.repository.ICommandRepository;
import ru.fedun.tm.api.repository.ICrudRepository;
import ru.fedun.tm.api.repository.IUserRepository;
import ru.fedun.tm.api.service.IAuthService;
import ru.fedun.tm.api.service.ICommandService;
import ru.fedun.tm.api.service.ICrudService;
import ru.fedun.tm.api.service.IUserService;
import ru.fedun.tm.constant.ArgumentConst;
import ru.fedun.tm.controller.*;
import ru.fedun.tm.entity.Project;
import ru.fedun.tm.entity.Task;
import ru.fedun.tm.exception.empty.EmptyCommandException;
import ru.fedun.tm.repository.CommandRepository;
import ru.fedun.tm.repository.ProjectRepository;
import ru.fedun.tm.repository.TaskRepository;
import ru.fedun.tm.repository.UserRepository;
import ru.fedun.tm.role.Role;
import ru.fedun.tm.service.*;
import ru.fedun.tm.util.TerminalUtil;

import static ru.fedun.tm.constant.TerminalConst.*;

public final class Bootstrap {

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthService authService = new AuthService(userService);

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ICrudRepository<Task> taskRepository = new TaskRepository();

    private final ICrudService<Task> taskService = new TaskService(taskRepository);

    private final ICrudController<Task> taskController = new TaskController(taskService, authService);

    private final ICrudRepository<Project> projectRepository = new ProjectRepository();

    private final ICrudService<Project> projectService = new ProjectService(projectRepository);

    private final ICrudController<Project> projectController = new ProjectController(projectService, authService);

    private final IAuthController authController = new AuthController(authService);

    private final IUserController userController = new UserController(userService, authService);

    private void initUsers() {
        userService.create("test", "test", "firstname", "lastname");
        userService.create("admin", "admin", "firstname", "lastname", Role.ADMIN);
    }

    public void run(final String[] args) {
        commandController.displayWelcome();
        if (parseArgs(args)) System.exit(0);
        initUsers();
        while (true) {
            try {
                runWithCommand(TerminalUtil.nextLine());
            } catch (Exception e) {
                System.err.println(e.getMessage());
                System.err.println("[FAIL]");
            }
        }
    }

    private void runWithCommand(final String command) {
        if (command == null || command.isEmpty()) throw new EmptyCommandException();
        switch (command) {
            case LOGIN:
                authController.login();
                break;
            case REGISTRY:
                authController.registry();
                break;
            case LOGOUT:
                authController.logout();
                break;
            case PROFILE:
                userController.showUserInfo();
                break;
            case DELETE_PROFILE:
                userController.deleteProfile();
                break;
            case CHANGE_EMAIL:
                userController.changeEmail();
                break;
            case CHANGE_PASSWORD:
                userController.changePassword();
                break;
            case VERSION:
                commandController.displayVersion();
                break;
            case ABOUT:
                commandController.displayAbout();
                break;
            case HELP:
                commandController.displayHelp();
                break;
            case INFO:
                commandController.displayInfo();
                break;
            case COMMANDS:
                commandController.displayCommands();
                break;
            case ARGUMENTS:
                commandController.displayArgs();
                break;
            case TASK_LIST:
                taskController.showAll();
                break;
            case TASK_CREATE:
                taskController.create();
                break;
            case TASK_CLEAR:
                taskController.clear();
                break;
            case PROJECT_LIST:
                projectController.showAll();
                break;
            case PROJECT_CREATE:
                projectController.create();
                break;
            case PROJECT_CLEAR:
                projectController.clear();
                break;
            case TASK_VIEW_BY_ID:
                taskController.showOneById();
                break;
            case TASK_VIEW_BY_INDEX:
                taskController.showOneByIndex();
                break;
            case TASK_VIEW_BY_TITLE:
                taskController.showOneByTitle();
                break;
            case TASK_UPDATE_BY_ID:
                taskController.updateOneById();
                break;
            case TASK_UPDATE_BY_INDEX:
                taskController.updateOneByIndex();
                break;
            case TASK_REMOVE_BY_ID:
                taskController.removeOneById();
                break;
            case TASK_REMOVE_BY_INDEX:
                taskController.removeOneByIndex();
                break;
            case TASK_REMOVE_BY_TITLE:
                taskController.removeOneByTitle();
                break;
            case PROJECT_VIEW_BY_ID:
                projectController.showOneById();
                break;
            case PROJECT_VIEW_BY_INDEX:
                projectController.showOneByIndex();
                break;
            case PROJECT_VIEW_BY_TITLE:
                projectController.showOneByTitle();
                break;
            case PROJECT_UPDATE_BY_ID:
                projectController.updateOneById();
                break;
            case PROJECT_UPDATE_BY_INDEX:
                projectController.updateOneByIndex();
                break;
            case PROJECT_REMOVE_BY_ID:
                projectController.removeOneById();
                break;
            case PROJECT_REMOVE_BY_INDEX:
                projectController.removeOneByIndex();
                break;
            case PROJECT_REMOVE_BY_TITLE:
                projectController.removeOneByTitle();
                break;
            case EXIT:
                commandController.exit();
            default:
                commandController.displayError();
        }

    }

    public boolean parseArgs(String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        runWithArg(arg);
        return true;
    }

    private void runWithArg(final String arg) {
        if (arg == null || arg.isEmpty()) {
            System.out.println("Empty command");
            return;
        }
        switch (arg) {
            case ArgumentConst.HELP:
                commandController.displayHelp();
                break;
            case ArgumentConst.ABOUT:
                commandController.displayAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.displayVersion();
                break;
            case ArgumentConst.INFO:
                commandController.displayInfo();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.displayArgs();
                break;
            case ArgumentConst.COMMANDS:
                commandController.displayCommands();
                break;
            default:
                commandController.displayError();
        }
    }

}
